#Method calls always return a value and we can pass that method call as
#an argument to another method based on the returned value 
def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def multiply(num1, num2)
  num1 * num2
end

puts multiply(add(20, 45), subtract(80, 10)) # returns 4550
