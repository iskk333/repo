#1. Method variable in method declaration is a PARAMETER
#2. Value that is passed to the active method in place of parameter is ARGUMENT

#3. To ensure that method always works WITH or WITHOUT arguments,
#we add a DEFAULT parameter inside it
def say(words='hello')
  puts words + '.'
end

say()
say("hi")
say "parethesis in methods are optional in Ruby"
#4. Parenthesis (...) are optional, but recommended in methods in Ruby
