#1. Generally methods can NOT modify arguments passed in to them
def Add(number)
  number = 7 # 1b. this is what returned by the method
end

a = 5
Add(a)
puts a
# 1c. But the puts result will be 5, because methods can't mutate the caller,
# because they are outside the method's scope.

#Example 2
a = [1, 2, 3]

def no_mutate(array)
  array.last
end
#This method doesn't mutate the caller, although should've returned last item
#of the array
print a
