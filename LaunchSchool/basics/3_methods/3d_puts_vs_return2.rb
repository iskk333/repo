#1. In Ruby, every method returns the evaluated result of the last line
#that was executed (implicit return),
def add_three(number)
  number + 3
end

returned_value = add_three(4)
puts returned_value

# 2. Unless an explicit 'return' comes before it
def add_three(number)
  return number + 3
  number + 4  #this line doesn't get executed, since there is return before it
end

returned_value = add_three(4)
puts returned_value #so the result will be 7 again
