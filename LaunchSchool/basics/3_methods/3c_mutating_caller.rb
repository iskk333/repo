# There are some Ruby built-in methods like that DO modify permanently
# variables(caller) from methods' scope.  We must lookup Ruby DOCS to make sure

a = [1, 2, 3]
def mutate(array)
  array.pop #This method can modify the caller
end

puts "Before mutate method: #{a}"
#Output will be [1, 2, 3]

mutate(a)
print "After mutate method: #{a}"
#Output will be [1, 2] Method changed an array permanently
