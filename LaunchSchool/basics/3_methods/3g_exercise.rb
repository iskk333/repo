#1. Simple method
def greeting(name)
  puts "Hello #{name}"
end
#If I put 'p' there is gonna be a return value, not 'nil'

greeting("Michael")

#2. What do the following expressions evaluate to?
#(Should be run in IRB)
x = 2  #returns value 2 (=> 2)

puts x = 2  # 'puts' method outputs value 2, but returns value 'nil'

p name = "Joe"
#'p' is NOT 'print' method and actually returns value - "Joe"
#that's the difference between 'p' and puts

four = "four" # returns value "four"

print something = "nothing"
#'print' behaves just like 'puts' method, except there is no new line
#it outputs 'nothing'value, but returns 'nil'
#any new code after it will be on the same line as 'print'

#3. 'multiply' method
def multiply(n1, n2)
  n1 * n2
end
puts multiply(4, 5) #We must use 'puts' otherwise nothing will show

#4. What will the following code print to the screen?
def scream(words)
  words = words + "!!!!"
  return
  puts words
end
scream("Yippeee")# Nothing will be printed to the screen
