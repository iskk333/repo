#Important fact: 'puts' returns 'nil'. if anywhere along the methods chain,
#there's a nil or an exception is thrown, the entire chained call will break down
def add_three(n)
  n + 3
end

add_three(5).times { puts "will this work?" }
# This works perfectly outputting message 8 times

#Example 2
def add_three(n)
  puts n + 3  ##But as soon as we put 'puts' we get 'nil' returned
end           #from a method resulting outputting 8 once and an error

add_three(5).times { puts "will this work?" }
