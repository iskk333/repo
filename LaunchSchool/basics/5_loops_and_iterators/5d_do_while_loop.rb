# Do..while 1
loop do
  puts "Do you want to do that again?"
  answer = gets.chomp
  if answer != 'Y'
    break
  end
end
#Loop will continue going until we are pressing 'Y'

# Do..while 2
begin
  puts "Do you want to do that again?"
  answer = gets.chomp
end while answer == 'Y'
#While the above works, it's not recommended by Matz, the creator of Ruby
