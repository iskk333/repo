x = gets.chomp.to_i
for i in 1..x do
  puts i
end
puts "Done!"
#Will return all numbers up until the number user enters

#Looping through an array
x = [1, 2, 3, 4, 5]
for i in x do
  puts i
end
puts "Done!"
