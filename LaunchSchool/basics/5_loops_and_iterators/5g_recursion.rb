#Recursion is another way to create a loop in Ruby
#Recursion is the act of calling a method from within itself

def doubler(start)
  puts start
  if start < 10
    doubler(start * 2)
  end
end
#Method 'doubler' is called again, passing it the doubled version of the
# value stored in the start variable

puts doubler(2)

def fibonacci(number)
  if number < 2
    number
  else
    fibonacci(number - 1) + fibonacci(number - 2)
  end
end

puts fibonacci(6)
