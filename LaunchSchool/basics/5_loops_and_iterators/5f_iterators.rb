#Iterators are methods that naturally loop over a given set of data
#and allow you to operate on each element in the collection.

#Most Rubyists, where possible, prefer iterators over loops.
names = ['Bob', 'Joe', 'Steve', 'Janice', 'Susan', 'Helen']

names.each { |name| puts name }
#Each time we iterate over the array, we need to assign
#the value of the element to a variable 'name' in our case

names = ['Bob', 'Joe', 'Steve', 'Janice', 'Susan', 'Helen']
x = 1

names.each do |name|
  puts "#{x}. #{name}"
  x += 1
end
