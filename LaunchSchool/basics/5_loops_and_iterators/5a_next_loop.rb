#Example of a simple loop
i = 0
loop do
  i += 1
  puts i
  if i == 10
    break
  end
end

#keyword 'next'
i = 0
loop do
  i += 2
  if i == 4
    next        # skip rest of the code and went to the beginning of loop
  end
  puts i
  if i == 10
    break
  end
end  #Output is 2 6 8 10
#code did not print out 4, because that was skipped over.
#Execution continued to the next iteration of the loop
