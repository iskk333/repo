# 1.
(32 * 4) >= 129 #false
false != !true #false
true == 4 #false. Didn't guess correctly
false == (847 == '874') #true
(!true || (!(100 / 5) == 20) || ((328 / 4) == 82)) || false  #true

# 2.
=begin
def change_string(input)
  if input.length > 10
    puts input.upcase
  else
    puts input
  end
end

change_string("hello world")
change_string("good")
=end

# 3.
#puts "Enter your number: "
#input = gets.to_i
def evaluate(input)
  if (input >= 0 && input <= 50)
    puts "Your number: #{input} is between 0 and 50"
  elsif (input >= 51 && input <= 100)
    puts "Your number: #{input} is between 51 and 100"
  else
    puts "Your number: #{input} is above 100"
  end
end

# 4.
=begin
'4' == 4 ? puts("TRUE") : puts("FALSE") #FALSE

x = 2
   if ((x * 3) / 2) == (4 + 4 - x - 3)
     puts "Did you get it right?" # this
   else
     puts "Did you?"
   end

y = 9
x = 10
   if (x + 1) <= (y)
     puts "Alright."
   elsif (x + 1) >= (y)
     puts "Alright now!" #this
   elsif (y + 1) == x
     puts "ALRIGHT NOW!"
   else
     puts "Alrighty!"
   end
=end
#5.
#puts "Enter your number: "
#input = gets.to_i

def evaluate_number(input)
  answer = case
  when input >= 0 && input <= 50
    puts "Your number: #{input} is between 0 and 50"
  when input >= 51 && input <= 100
    puts "Your number: #{input} is between 51 and 100"
  else
    puts "Your number: #{input} is above 100"
  end
end

#5b. Calling methods from exercise 3 and 5
evaluate(3)
evaluate_number(101)

#5e. Elegant solution to exercise 5 from book
def evaluate_num(num)
  if num < 0
    puts "You can't enter a negative num!"
  elsif num <= 50
    puts "#{num} is between 0 and 50"
  elsif num <= 100
    puts "#{num} is between 51 and 100"
  else
    puts "#{num} is above 100"
  end
end

def evaluate_case1_num(num)
  case
  when num < 0
    puts "You can't enter a negative num!"
  when num <= 50
    puts "#{num} is between 0 and 50"
  when num <= 100
    puts "#{num} is between 51 and 100"
  else
    puts "#{num} is above 100"
  end
end

def evaluate_case2_num(num)
  case num
  when 0..50
    puts "#{num} is between 0 and 50"
  when 51..100
    puts "#{num} is between 51 and 100"
  else
    if num < 0
      puts "You can't enter a negative num!"
    else
      puts "#{num} is above 100"
    end
  end
end

puts "Please enter a number between 0 and 100."
number = gets.chomp.to_i

evaluate_num(number)
evaluate_case1_num(number)
evaluate_case2_num(number)
