puts "Put in a number: "
a = gets.to_i
#We need to convert user input into integer, cause 'gets' always gives 
#us a string.

if a == 3
  puts "a is 3"
elsif a == 4
  puts "a is 4"
else
  puts "#{a} is neither 3, nor 4"
end
