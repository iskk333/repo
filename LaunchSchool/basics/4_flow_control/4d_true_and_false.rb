#In Ruby, every expression evaluates to true when used in flow control,
#except for 'false' and 'nil'
a = 'false'
if a
  puts "this is true" #this will be the output
else
  puts "it is not true"
end

#Example2
if x = 5 #Don't mistake with x == 5
  puts "how can this be true?"
else
  puts "it is not true"
end
