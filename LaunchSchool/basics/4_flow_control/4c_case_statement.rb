# Case statements use the reserved words 'case', 'when', 'else', and 'end'
a = 5

case a
when 5
  puts "a is 5"
when 6
  puts "a is 6"
else
  puts "a is neither 5 or 6"
end

# We can also save the result of a case statement into a variable
a = 5
answer = case a #argument 'a' is optional if we check 'a' in each case
when a == 5
  puts "a is 5"
when a == 6
  puts "a is 6"
else
  puts "a is neither 5 or 6"
end

puts answer
