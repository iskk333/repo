# 'puts' only prints to the screen and returns 'nil'
puts "Ruby only prints to the screen"
#  'nil' is Ruby's way of saying 'nothing', was returned

a = puts "stuff"
puts a
# We still get 'nil'
