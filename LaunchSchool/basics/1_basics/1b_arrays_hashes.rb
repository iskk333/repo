#in Arrays and Hashes [ ] used to retrieve value
array = [ 1, 2, 3, 4, 5]
puts array[1]

animals = {:dog => 'barks', :cat => 'meows', :pig => 'oinks'}
puts animals[:cat]

#Different ways to place : and =>, when entering key/value pairs
key_value = { :key=> "value", key2: "value"}
#colon is used differently on two keys

puts key_value[:key]
puts key_value[:key2]
