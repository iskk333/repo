# When using just gets for user input we will get returned  => "Bek\n" in IRB
input = gets
puts input

#To get rid of ugly '\n' we use 'chomp' method making it => "Bek" in IRB
input = gets.chomp
puts input
