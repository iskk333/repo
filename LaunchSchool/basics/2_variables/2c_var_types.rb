#CONSTANTS
#- Can't be declared in methods
#- Are available throughout application's scopes
#- Change of value is allowed in Ruby, but with a warning message
MY_CONSTANT = 'I am available throughout your app.'

#GLOBAL variables:
#- Available throughout our entire app
#- Rubyists tend to stay away from global variables as there can be unexpected
# complications when using them
$var = 'I am also available throughout your app.'

#CLASS variables
#- Are accessible by instances of your class and class itself
#- Used for declaring a variable that is related to a class, but each
#instance of that class does NOT need its own value for this variable
#- Must be initialized at the class level, outside of any methods
#- Can then be altered using class or instance methods
@@instances = 0

#INSTANCE variables
#- Are available throughout the current instance of the parent class
#- Can cross some scope boundaries, but not all of them
#- Very much related to OOP
@var = 'I am available throughout the current instance of this class.'

#LOCAL variables
#- Are the most common variables
#- Obey all scope boundaries
var = 'I must be passed around to cross scope boundaries.'
