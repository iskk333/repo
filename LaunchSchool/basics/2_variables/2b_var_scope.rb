
# EXAMPLE 1
a = 5        # variable is initialized in the outer scope

3.times do |n|
  a = 3      # is 'a' accessible here, in an inner scope?
end

puts a # value of 'a' is 3
# 'for ...do/end' code does NOT create a new inner scope for a variable,
# making it possible to re-assing 'a' with 3
# 'for ...do/end' structure is a part of Ruby language and not a method (called)

# EXAMPLE 2
a = 5

3.times do |n|
  a = 3
  b = 5      # b is initialized in the inner scope
end

puts a
puts b       # is b accessible here, in the outer scope?

# Error for 'b' - undefined local variable or method `b'
# 'each', 'times' and other methods, followed by { } or do/end
# DO create inner scope for variables
