﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CS_006.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-family: Arial, Helvetica, sans-serif;
        }
        .auto-style2 {
            color: #FF0000;
        }
        .auto-style3 {
            width: 100%;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            height: 23px;
            width: 527px;
        }
        .auto-style6 {
            width: 527px;
        }
        .auto-style7 {
            background-color: #FFFF99;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h1>Head Line 1</h1>
        <h2>Head Line 2</h2>
        <h3>Head Line 3</h3>
        <h4>Head Line 4</h4>
        <h5>Head Line 5</h5>
        <h6>Head Line 6</h6>
    
    </div>
        <p class="auto-style1">
            This is some text that I want to<span class="auto-style2"> apply</span> a style to</p>
        <p class="auto-style1">
            <a href="http://www.google.com">Add a hyperlink</a></p>
        <p class="auto-style1">
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="www.google.com" Target="_blank">This is another hyperlink.</asp:HyperLink>
        </p>
        <p class="auto-style1">
            &nbsp;</p>
        <p class="auto-style1">
            <asp:Image ID="Image1" runat="server" Height="297px" ImageUrl="~/Launch school learning tips.JPG" style="margin-right: 0px" Width="429px" />
        </p>
        <table class="auto-style3">
            <tr>
                <td class="auto-style5">Player</td>
                <td class="auto-style4">Year</td>
                <td class="auto-style4">Homeruns</td>
            </tr>
            <tr>
                <td class="auto-style6">Sammy Sosa</td>
                <td>2005</td>
                <td>100</td>
            </tr>
            <tr>
                <td class="auto-style6">Mark MacGuire</td>
                <td>2005</td>
                <td>102</td>
            </tr>
        </table>
    </form>
    <p>
        &nbsp;</p>
    <ol>
        <li>First item</li>
        <li>Second item</li>
        <li>Third item</li>
    </ol>
    <ul>
        <li class="auto-style7">Buy food</li>
        <li class="auto-style7">Pay bills</li>
        <li class="auto-style7">Go to the movies</li>
    </ul>
</body>
</html>
