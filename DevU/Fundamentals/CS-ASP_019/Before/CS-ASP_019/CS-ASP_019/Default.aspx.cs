﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_019
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void submitButton_Click(object sender, EventArgs e)
        {
            //string result = String.Format("Thank you, {0}, for your business", nameTextBox.Text); //! Simple name formatting wit replacement code { }
            #region 1. Formatting text box entry into SSN/Tel# style formatting
            //! 1a. We first convert string to int, because this type of formatting only works with ints
            int ss = int.Parse(ssTextBox.Text);

            //! 1b. And then pass that var as arguments
            //! 1c. Now to formatted to SSN style we add to replacement code :000-00-0000. For tel# formatting :(000) 000-0000 
            //! 1d. We can use any HTML tags like <br /> to go to the next line. It's not gonna work in code for Desktop or Concole applications
            //string result = String.Format("Thank you, {0}, for your business. <br />Your SS# is: {1:000-00-0000}", nameTextBox.Text, ss);
            #endregion
            #region 2. Formatting text box entry into Date formatting. Reference docs: http://is.gd/formattingdates
            //! 2a. We grab the date and add it to a string using Calendar Control
            //! 2b. Without date formatting result will be '1/2/2017 12:00:00 AM'
            //string result = String.Format("Thank you, {0}, for your business. " + 
                "<br />Your SS# is: {1:000-00-0000}" + 
                "<br />Loan Date: {2:ddd --d, M, yy}",  //! 2c. Result will be 'Mon --2, 1, 17'
                nameTextBox.Text, ss, loanDateCalendar.SelectedDate);
            #endregion
            #region 3. Formatting currency. Reference docs: http://is.gd/formattingcurrency
            //! 3a. Default formatting has no $ sign and is really numbers without comma or dot

            //! 3b. To start working with currency, we need to have salary entry converted from text to double data type
            double salary = double.Parse(salaryTextBox.Text); 

            string result = String.Format("Thank you, {0}, for your business. " + 
            "<br />Your SS# is: {1:000-00-0000}" +
            "<br />Loan Date: {2:ddd --d, M, yy}" +
            "<br />Salary: {3:C}",  //! 3c. It will format it to $50,000.00 
             nameTextBox.Text,ss, loanDateCalendar.SelectedDate, salary);
            #endregion

            resultLabel.Text = result;
        }
    }
}