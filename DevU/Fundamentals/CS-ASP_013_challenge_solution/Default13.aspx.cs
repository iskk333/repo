﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_013_challenge_solution
{
    public partial class Default13 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void purchaseBT_Click(object sender, EventArgs e)
        {
            // Size of Pizza
            double price = 0;
            double pepperoni = 1.50;
            double onions = 1.75;
            double greenPeppers = 0.50;
            double redPeppers = 0.75;
            double anchovies = 2.00;

            if (babyRB.Checked)
                price = 10.00;
            else if (mamaRB.Checked)
                price = 13.00;
            else if (papaRB.Checked)
                price = 16.00;
            

            // Crust of Pizza
            if (deepRB.Checked) price += 2.00;
            else totalLabel.Text = "You must choose pizza crust!";
            // Pizza toppings
            if (pepperoniCB.Checked) price += pepperoni;
            if (onionsCB.Checked) price += onions;
            if (gPeppersCB.Checked) price += greenPeppers;
            if (rPeppersCB.Checked) price += redPeppers;
            if (anchCB.Checked) price += anchovies;
            // Deal
            if (pepperoniCB.Checked && gPeppersCB.Checked && anchCB.Checked ||
                pepperoniCB.Checked && rPeppersCB.Checked && onionsCB.Checked)
                price -= 2.00;
            string totalPrice = Convert.ToString(price);

            if (!babyRB.Checked && !mamaRB.Checked && !papaRB.Checked)
                totalLabel.Text = "You must choose pizza size";
            else if ((babyRB.Checked || mamaRB.Checked || papaRB.Checked) && (!thinRB.Checked && !deepRB.Checked))
                totalLabel.Text = "You must choose pizza crust";
            else
                totalLabel.Text = "Total $" + totalPrice;
        }
    }
}