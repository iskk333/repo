﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default13.aspx.cs" Inherits="CS_ASP_013_challenge_solution.Default13" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-family: Arial, Helvetica, sans-serif;
        }
        .auto-style2 {
            width: 200px;
            height: 200px;
        }
        .auto-style3 {
            color: #CC3300;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="auto-style1">
    
        <h1><strong>
            <img alt="Papa Bob's logo" class="auto-style2" src="PapaBob.png" />Papa Bob&#39;s Pizza and Software</strong></h1>
        <p>
            &nbsp;</p>
    
    </div>
        <asp:RadioButton ID="babyRB" runat="server" GroupName="pizzaSize" Text="Baby Bob Size (10&quot;) -$10" />
        <br />
        <asp:RadioButton ID="mamaRB" runat="server" GroupName="pizzaSize" Text="Mama Bob Size(13&quot;) -$13" />
        <br />
        <asp:RadioButton ID="papaRB" runat="server" GroupName="pizzaSize" Text="Papa Bob Size (16&quot;) - $16" />
        <br />
        <br />
        <asp:RadioButton ID="thinRB" runat="server" GroupName="crust" Text="Thin Crust" />
        <br />
        <asp:RadioButton ID="deepRB" runat="server" GroupName="crust" Text="Deep Dish (+$2)" />
        <p>
            <asp:CheckBox ID="pepperoniCB" runat="server" Text="Pepperoni (+$1.50)" />
        </p>
        <p>
            <asp:CheckBox ID="onionsCB" runat="server" Text="Onions (+$0.75)" />
        </p>
        <p>
            <asp:CheckBox ID="gPeppersCB" runat="server" Text="Green Peppers (+$0.50)" />
        </p>
        <p>
            <asp:CheckBox ID="rPeppersCB" runat="server" Text="Red Peppers (+$0.75)" />
        </p>
        <p>
            <asp:CheckBox ID="anchCB" runat="server" Text="Anchovies (+$2)" />
        </p>
        <h3>Papa Bob&#39;s <span class="auto-style3">Special Deal</span></h3>
        <p>
            Save $2.00 when you add Pepperoni, Green Peppers and Anchovices OR Pepperoni, Red Peppers and Onions to your pizza.</p>
        <p>
            <asp:Button ID="purchaseBT" runat="server" OnClick="purchaseBT_Click" Text="Purchase" />
        </p>
        <p>
            <asp:Label ID="totalLabel" runat="server"></asp:Label>
        </p>
        <p>
            &nbsp;</p>
        <p>
            &nbsp;</p>
    </form>
</body>
</html>
