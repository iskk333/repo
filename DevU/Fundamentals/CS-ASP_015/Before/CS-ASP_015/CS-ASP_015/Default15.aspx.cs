﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_015
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void okButton_Click(object sender, EventArgs e)
        {
            #region 1. Creating TimeSpan object using 'Parse()'
            TimeSpan myTimeSpan = TimeSpan.Parse("1.2:3:30.5"); //! Days.Hours:Minutes:Seconds.Milliseconds - arguments formula
            #endregion

            #region 2. Creating TimeSpan object after creating base DateTime object
            //? It will be result of some helper method (Subtract for example) on DateTime object
            DateTime newCentury = DateTime.Parse("01/01/2000");   //! 1. We create DateTime object with a specific date
            TimeSpan age = DateTime.Now.Subtract(newCentury); //! 2. We create TimeSpan object that subtracts that specific date from Now property
            #endregion

            #region Getting individual parts of TimeSpan object displayed
            resultLabel.Text = age.Hours.ToString(); //! result is '11' (Not total hours) That's 11 hours from begining of the day (00:00 am)
            resultLabel.Text = age.TotalDays.ToString();
            //! TotalDays property counts days in double format: 6206.48453223343 after dot are hrs, min, sec, milsec till this moment of running code
            #endregion

        }
    }
}