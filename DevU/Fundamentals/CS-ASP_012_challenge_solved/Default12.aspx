﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default12.aspx.cs" Inherits="CS_ASP_012_challenge_solved.Default12" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        Your note taking preferences<br />
        <br />
        <asp:RadioButton ID="pencilRadioButton" runat="server" GroupName="NoteTakingPref" Text="Pencil" />
        <br />
        <asp:RadioButton ID="penRadioButton" runat="server" GroupName="NoteTakingPref" OnCheckedChanged="RadioButton2_CheckedChanged" Text="Pen" />
        <br />
        <asp:RadioButton ID="phoneRadioButton" runat="server" GroupName="NoteTakingPref" Text="Phone" />
        <br />
        <asp:RadioButton ID="tabletRadioButton" runat="server" GroupName="NoteTakingPref" Text="Tablet" />
        <br />
        <br />
    
    </div>
        <asp:Button ID="okButton" runat="server" OnClick="okButton_Click" Text="OK" />
        <br />
        <br />
        <asp:Label ID="resultLabel" runat="server"></asp:Label>
        <br />
        <asp:Image ID="image" runat="server" />
    </form>
</body>
</html>
