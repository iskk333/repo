﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_012_challenge_solved
{
    public partial class Default12 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        protected void okButton_Click(object sender, EventArgs e)
        {
            if (pencilRadioButton.Checked)
                resultLabel.Text = "You selected Pen";
            else if (penRadioButton.Checked)
                resultLabel.Text = "You selected Pen";
            else if (phoneRadioButton.Checked)
                resultLabel.Text = "You selected Phone";
            else if (tabletRadioButton.Checked)
                resultLabel.Text = "You selected Tablet";
            else
                resultLabel.Text = "Please select an option";

            if (pencilRadioButton.Checked)
                image.ImageUrl = "Assets/pencil.png";
            else if (penRadioButton.Checked)
                image.ImageUrl = "Assets/pen.png";
            else if (phoneRadioButton.Checked)
                image.ImageUrl = "Assets/phone.png";
            else if (tabletRadioButton.Checked)
                image.ImageUrl = "Assets/tablet.png";
            else
                resultLabel.Text = "Please select an option";
        }

    }
}