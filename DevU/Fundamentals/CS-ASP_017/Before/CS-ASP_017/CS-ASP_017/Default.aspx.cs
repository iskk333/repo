﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_017
{
    public partial class Default : System.Web.UI.Page
    {
        #region 4. Docs at: http://is.gd/pagelifecycle about order of events firing
        #endregion
        #region 1. What is Page_Load
        //! Page_Load is an event, that loads first initializes values of server controls on a web form
        //! It resets whatever values user typed-in or selcted on a calendar back to its values until we put appropriate
        #endregion
        protected void Page_Load(object sender, EventArgs e) 
        {
            #region 5. What is IsPostBack? (05:34). It's a property, that checks page render for the 1st time. 
            //! We are checking if it's true that we are NOT posting back. If it's, we will use initialized values below 
            //! If this page is loading again (we clicked OK btn, refreshed), than means we are posting back IsPostBack and code below will be ignored 
            #endregion
            if (!Page.IsPostBack) 
            {
                myTextBox.Text = "Some value";
                myCalendar.SelectedDate = DateTime.Now.Date.AddDays(2);
                #region 2. Why Date property is next to Now
                //! 2. If we drop Date property, we'll see no selected date in a calendar, because Now represents also time now and calendar only
                //! works at exactly 00:00 am value, so if now is not midnight, SelectedDate won't work. That's why we extract specifically
                //! Date part of DateTime.Now, so time part of it doesn't mess it up. We can also use Today instead of Now.Date
                #endregion
            }

        }

        protected void okButton_Click(object sender, EventArgs e)
        {
            # region 3. User can enter his choice of values, but as soon as button is pressed, values will be set to the ones on Page_Load 
            #endregion
            resultLabel.Text = myTextBox.Text + " - " 
                + myCalendar.SelectedDate.ToShortDateString();
        }
    }
}