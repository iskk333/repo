﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_016
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void getDateButton_Click(object sender, EventArgs e)
        {   //! We set ID of our calendar as myCalendar in properties
            //! SelectedDate is a property in Misc section of our calendar control
            //! Date we select on a calendar (becomes diff color when clicked) will show as text when Get Date button is pressed
            resultLabel.Text = myCalendar.SelectedDate.ToShortDateString(); // Notice that there is no variables assignment

        }

        protected void setDateButton_Click(object sender, EventArgs e)
        {
            //! Here we assign SelectDate property a SPECIFIC DATE (will show as diff color on a calendar than today's date)
            myCalendar.SelectedDate = DateTime.Parse("12/01/2016");
            //! We can then press Get Date button again to output parsed Parse() date onto a label like we did above
        }

        protected void showDateButton_Click(object sender, EventArgs e)
        {
            //! We can program this button to go to a certain month and year (not a certain day) on a calendar using VisibleDate property
            myCalendar.VisibleDate = DateTime.Parse("12/01/2000");
        }

        protected void selecedWeekButton_Click(object sender, EventArgs e)
        {
            //! Didn't find this option useful
        }

        //! We can output onto label selected date without pressing a button, using Events property window (lighting icon in Properties)
        //! Inside myCalendar's Action tab, we double click on SelectionChanged property, that generates method below
        protected void myCalendar_SelectionChanged(object sender, EventArgs e)
        {
            resultLabel.Text = myCalendar.SelectedDate.ToShortDateString();
            //! And here we just indicate what SelectedDate and in what format ToShortDateString() will be outputed on the label
        }


    }
}