﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_challenge_solution
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //! My solution that worked
            /*
            DateTime firstChoice = calendarOne.SelectedDate;
            DateTime secondChoice = calendarTwo.SelectedDate;
            int extractOne = firstChoice.Day;
            int extractTwo = secondChoice.Day;
            int result;

            if (extractOne > extractTwo)
            {
                result = extractOne - extractTwo;
                resultLabel.Text = result.ToString();
            }
            else if (extractOne < extractTwo)
            {
                result = extractTwo - extractOne;
                resultLabel.Text = result.ToString();
            }
            */
            //! Bob's solution
            if(calendarOne.SelectedDate > calendarTwo.SelectedDate)
            {
                resultLabel.Text = calendarOne.SelectedDate.Subtract(calendarTwo.SelectedDate).TotalDays.ToString();
                //Subtract() is self explanatory
                //TotalDays property returns the total number of days
            }
            else
            {
                resultLabel.Text = calendarTwo.SelectedDate.Subtract(calendarOne.SelectedDate).TotalDays.ToString();
            }

        }
    }
}