﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_010
{
    public partial class Default : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void subtract_Click(object sender, EventArgs e)
        {
            int operandOneText = int.Parse(operandOneTextBox.Text);
            int operandTwoText = int.Parse(operandTwoTextBox.Text);            
            int result = operandOneText - operandTwoText;
            resultLabel.Text = result.ToString();
        }

        protected void multiply_Click(object sender, EventArgs e)
        {
            int operandOneText = int.Parse(operandOneTextBox.Text);
            int operandTwoText = int.Parse(operandTwoTextBox.Text);
            int result = operandOneText + operandTwoText;
            resultLabel.Text = result.ToString();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            int operandOneText = int.Parse(operandOneTextBox.Text);
            int operandTwoText = int.Parse(operandTwoTextBox.Text);
            int result = operandOneText * operandTwoText;
            resultLabel.Text = result.ToString();
        }

        protected void divide_Click(object sender, EventArgs e)
        {
            int operandOneText = int.Parse(operandOneTextBox.Text);
            int operandTwoText = int.Parse(operandTwoTextBox.Text);
            float result = operandOneText - operandTwoText;
            resultLabel.Text = result.ToString();
        }
}