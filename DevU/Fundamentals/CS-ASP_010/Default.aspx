﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CS_ASP_010.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h2>Simple Calculator</h2>
        <p>
            <span class="auto-style1">First value:</span>&nbsp;
            <asp:TextBox ID="operandOneTextBox" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
        </p>
        <p>
            <span class="auto-style1">Second value:</span>
            <asp:TextBox ID="operandTwoTextBox" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Button ID="add" runat="server" OnClick="multiply_Click" Text="+" />
&nbsp;
            <asp:Button ID="subtract" runat="server" OnClick="subtract_Click" Text="-" />
&nbsp;
            <asp:Button ID="multiply" runat="server" OnClick="Button3_Click" Text="*" />
&nbsp;
            <asp:Button ID="divide" runat="server" OnClick="divide_Click" Text="/" />
        </p>
        <p>
            <asp:Label ID="resultLabel" runat="server" BackColor="#99CCFF" Font-Bold="True"></asp:Label>
        </p>
        <p>
            &nbsp;</p>
    
    </div>
    </form>
</body>
</html>
