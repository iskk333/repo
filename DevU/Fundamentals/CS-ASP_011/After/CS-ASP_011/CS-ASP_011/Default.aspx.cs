﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_011
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void okButton_Click(object sender, EventArgs e)
        {
            resultLabel.Text = ""; //! This is done, so after each try label will be cleared

            #region 1. IF/ELSE
            * /*
            if (firstTextBox.Text == secondTextBox.Text)
            {
                resultLabel.Text = "Yes!  They're equal!";
            }
            else
            {
                resultLabel.Text = "No! They're not equal!";
            }
            */
            #endregion
            #region 2. Checkbox button: 06:10 
            //! 2a. Bob initially added '== true' to the condition, but it's actually not needed, because 
            //! there is a property on checkbox control that has values 'true' or 'false'  
             /*
            if (coolCheckBox.Checked) 
            {
                resultLabel.Text = "Yes, you are definitely cool.";
            }
            else
            {
                resultLabel.Text = "Don't be so hard on yourself.";
            }
            */
            #endregion

            #region 3. Radio button: 10:00
            
            //! 3a. See notes for 'GroupName' property
            if (pizzaRadioButton.Checked) 
            {
                resultLabel.Text = "You must be from Chicago!";
            }
            else if (saladRadioButton.Checked)
            {
                resultLabel.Text = "You must be healthy";
            }
            else if (pbjRadioButton.Checked)
            {
                resultLabel.Text = "You must be a fun loving person!";
            }
            else
            {
                resultLabel.Text = "Please select one of the options.";
            }
            #endregion
        }
    }
}