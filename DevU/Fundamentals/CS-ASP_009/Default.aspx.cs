﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_009
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void okButton_Click(object sender, EventArgs e)
        {
          
            //int myInteger = 7;
            //double myDouble = 5.5;
            //double myResult = myDouble + myInteger;
            //! Result is 12.5, becasue there is an IMPLICIT upcast when we declare a var type double 'myResult'

            //double myDouble = 5.5;
            //double myResult = myDouble + myInteger;
            //int myResult = (int)myDouble + myInteger;
            //! Result is 12, because there was an EXPLICIT downcast to int, when we changed type double tp int '(int)myDouble'
            //! var type double lost everything after a dot during an explicit cast

            //int myOtherInteger = 4;
            //! When we deal with division precision matters and we'll probably need to deal with explicit casting
            //int myResult = myInteger / myOtherInteger;
            //! Result will be 1, because we deal with whole numbers, but the actul result will be 1.75

            //double myResult = myInteger / (double)myOtherInteger;
            //! Result will be 1, because 1 in devided by the other int first, and then it was assigned to var type double

            //double myResult = (double)myInteger / (double)myOtherInteger;
            //! Here the result will be correct, because we explicitely casted 

            //double myResult = (double)myInteger / myOtherInteger;
            //! Result is also 1.75. Casting of just 1 operand is enough to get the right double type result
            resultLabel.Text = myResult.ToString();

            //! Another thing to be aware of is OVERFLOW from the data type ( during + and *). 
            //! Here is an example of int overflowing 
            int firstNumber = 2000000000;
            int secondNumber = 2000000000;
            //int resultNumber = firstNumber * secondNumber;
            resultLabel.Text = resultNumber.ToString();
            //! Result will be - 161507200, because result (binary representation )of overflow will be larger than max value for the type and  
            //! will have 1 at the beginning position, which is considred to be a negative number 
            //! Overflow result like - 161507200 are called QUIET ERRORS. They are very dangerous, because they don't cause runtume errors

            long resultNumber;
            //! Even long can't hold the result of billions on billions multiplication

            //! This operation will give us 'overflow exception' thus catching and revealing a quiet error
            //! There is a way to handle exceptions, BUT there is no way to handle QUIET ERRORS
            checked
            {
                resultNumber = firstNumber * secondNumber;
            }

            resultLabel.Text = resultNumber.ToString();
            

        }
    }
}