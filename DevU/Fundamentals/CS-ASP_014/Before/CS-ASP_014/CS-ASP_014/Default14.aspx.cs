﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS_ASP_014
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void okButton_Click(object sender, EventArgs e)
        {
            #region 1. Creating DateTime object using 'Now'

            DateTime myValue = DateTime.Now; //! set it to the moment whenever this line of code is executed (when OK button is pressed)
            #endregion

            #region 2. Creating new DateTime object with a specific date (13:00)
            DateTime myValue = DateTime.Parse("01/01/2000"); //! This is One way of creating using Parse()
            DateTime myValue = new DateTime(2000, 12, 7); //! This is Another way using ctor. We can pass in year, month, day 
            DateTime myValue = new DateTime(2000, 12, 7, 13, 30, 0); //! And in addition to those: hour, min, sec
            #endregion

            #region Formatting DateTime object using many options of helper methods 'myDateTimeObject.To________()'
            resultLabel.Text = myValue.ToString(); //! To avoid error, we explicitely convert it to string
            resultLabel.Text = myValue.ToLongDateString(); //! Outputs 'Tuesday, December 27, 2016' Time doesn't show using this method
            resultLabel.Text = myValue.ToLongTimeString(); //! '11:14:14 PM' Only time is shown when we use this method
            resultLabel.Text = myValue.ToShortDateString(); //! '12/27/2016'
            resultLabel.Text = myValue.ToShortTimeString(); //! '11:17 PM' The difference with 'LongTimeString' is no seconds
            #endregion

            #region Adding to DateTime object using 'myDateTimeObject.Add_____()'
            //? In addition to formating a string for display, we can also add seconds, minutes, hours, days, months, year to 
            //? existing time (Now)
            resultLabel.Text = myValue.AddDays(2).ToString(); //! We're adding 2 days from code execution moment. 2 helper methods chained 
            resultLabel.Text = myValue.AddMonths(-2).ToString(); //! Today is 12/27.To subtract,we put negative number '10/27/2016 11:26:40 PM'
            #endregion

            #region Retrieving parts of DateTime object 
            //? We can retrieve specific parts of days as integers, so we can perform math on them (including comparison)
            resultLabel.Text = myValue.Month.ToString(); //! 'Month' is property. Output will be 6

            //? We can retrieve specifically any component of DateTime object , even millisecond
            resultLabel.Text = myValue.IsDaylightSavingTime().ToString(); //! 'bool' value is returned. Need to convert 'ToString()'
            resultLabel.Text = myValue.DayOfWeek.ToString(); //! What day of the week is it. 'Tuesday'   
            resultLabel.Text = myValue.DayOfYear.ToString(); //! How far into the year are we. '175'
            #endregion








        }
    }
}